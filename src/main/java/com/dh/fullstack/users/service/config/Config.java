package com.dh.fullstack.users.service.config;

import com.dh.fullstack.users.service.bean.Asus;
import com.dh.fullstack.users.service.bean.Prueba;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * @author Santiago Mamani
 */
@Configuration
public class Config {

    @Bean
    @Scope("prototype")
    public Asus beanAsus(){
        Asus asus = new Asus();
        asus.setName("I am asus");

        return asus;
    }

    @Bean
    @Scope("prototype")
    public Prueba beanPrueba(){
        Prueba prueba = new Prueba();
        prueba.setName("Esto es un bean");

        return prueba;
    }
}
